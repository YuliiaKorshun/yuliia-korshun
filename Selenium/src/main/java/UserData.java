import java.util.Random;

public class UserData {
    final Random random = new Random();
    int randNum = random.nextInt(87)+11;

    private String firstName = "Yuliia" + randNum;
    private String lastName = "Korshun" + randNum;
    private String workPhone = "451325" + randNum;
    private String mobilePhone = "3805024038" + randNum;
    private String email = "korshun" + randNum+"@gmail.com";
    private String password = "Password!" + randNum;


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
