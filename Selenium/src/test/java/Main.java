import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;


public class Main{
    String url = "https://user-data.hillel.it/html/registration.html";
    WebDriver driver = new ChromeDriver();
    WebDriverWait wait = new WebDriverWait(driver, 5);
    UserData userData = new UserData();

    @BeforeClass
    public static void BeforeClass(){
        final String path = String.format("%s/bin/chromedriver.exe",
                System.getProperty("user.dir"));
        System.setProperty("webdriver.chrome.driver", path);
    }

    public void selectPosition(String role){
        Select position = new Select(driver.findElement(By.id("position")));
        position.selectByValue(role);
    }

    public void selectGender(String gender){
        Select position = new Select(driver.findElement(By.id("gender")));
        position.selectByVisibleText(gender);
    }


    @Test
    public void firstTest() {
        driver.get(url);
        driver.findElement(By.className("registration")).click();
        driver.findElement(By.id("first_name")).sendKeys(userData.getFirstName());
        driver.findElement(By.id("last_name")).sendKeys(userData.getLastName());
        driver.findElement(By.id("field_work_phone")).sendKeys(userData.getWorkPhone());
        driver.findElement(By.id("field_phone")).sendKeys(userData.getMobilePhone());
        driver.findElement(By.id("field_email")).sendKeys(userData.getEmail());
        driver.findElement(By.id("field_password")).sendKeys(userData.getPassword());
        driver.findElement(By.id("female")).click();
        selectPosition("developer");
        driver.findElement(By.className("create_account")).click();
        wait.until(ExpectedConditions.alertIsPresent());
        driver.switchTo().alert().accept();
        driver.navigate().refresh();
        driver.findElement(By.id("email")).sendKeys(userData.getEmail());
        driver.findElement(By.id("password")).sendKeys(userData.getPassword());
        driver.findElement(By.className("login_button")).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.id("employees")));
        driver.findElement(By.id("employees")).click();
        driver.findElement(By.id("first_name")).sendKeys(userData.getFirstName());
        driver.findElement(By.id("last_name")).sendKeys(userData.getLastName());
        driver.findElement(By.id("mobile_phone")).sendKeys(userData.getMobilePhone());
        selectPosition("developer");
        selectGender("female");
        driver.findElement(By.id("search")).click();
        List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + userData.getEmail() + "')]"));
        Assert.assertTrue("Text not found!", list.size() == 1);
        Assert.assertEquals("wrong email",userData.getEmail(), driver.findElement(By.xpath("//*[contains(text(),'" + userData.getEmail() + "')]")).getText());
        Assert.assertEquals("wrong first name",userData.getFirstName(), driver.findElement(By.xpath("//*[contains(text(),'" + userData.getFirstName() + "')]")).getText());
        Assert.assertEquals("wrong last name",userData.getLastName(), driver.findElement(By.xpath("//*[contains(text(),'" + userData.getLastName() + "')]")).getText());
        Assert.assertEquals("wrong work phone",userData.getWorkPhone(), driver.findElement(By.xpath("//*[contains(text(),'" + userData.getWorkPhone() + "')]")).getText());
        Assert.assertEquals("wrong mobile phone",userData.getMobilePhone(), driver.findElement(By.xpath("//*[contains(text(),'" + userData.getMobilePhone() + "')]")).getText());
        Assert.assertEquals("wrong role","developer", driver.findElement(By.xpath("//*[contains(text(),'developer')]")).getText());
        Assert.assertEquals("wrong gender","female", driver.findElement(By.xpath("//*[contains(text(),'female')]")).getText());
    }

    @After
    public void postConditions(){
        driver.quit();
    }

}
