package Objects;

public class User {
    private String email;
    private String password;
    private String firstname;
    private String secondname;
    private String mobilephone;
    private int workphone;

    Validator validator = new Validator();

    public User(String email, String password){
        this.email = validator.checkEmail(email);
        this.password = validator.checkPassword(password);
    }

    public User(String email, String password, String firstname, String secondname, String mobilephone, int workphone) {
        this.email = validator.checkEmail(email);
        this.password = validator.checkPassword(password);
        this.firstname = firstname;
        this.secondname = secondname;
        this.mobilephone = mobilephone;
        this.workphone = workphone;
    }
}
