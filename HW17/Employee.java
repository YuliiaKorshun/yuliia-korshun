package Objects;

public class Employee extends User {
    private int salary;
    private int experience;

    public Employee(String email, String password, int salary, int experience) {
        super(email, password);
        this.salary = salary;
        this.experience = experience;
    }

    public Employee(String email, String password, String firstname, String secondname, String mobilephone, int workphone, int salary, int experience) {
        super(email, password, firstname, secondname,mobilephone, workphone);
        this.salary = salary;
        this.experience = experience;
    }

    public void salaryRaise(int salary, int experience){
        if (experience >0 & experience < 2){
            salary +=(salary * 0.1);
            System.out.println(salary);
        }
        else if(experience>=2 & experience<=5){
            salary+=(salary * 0.1);
            System.out.println(salary);
        }
        else if(experience>5){
            salary+=(salary * 0.15);
            System.out.println(salary);
        }
    }

    public int getExperience() {
        return experience;
    }

    public int getSalary() {
        return salary;
    }

}
